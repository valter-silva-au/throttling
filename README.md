# Throttling
---

You will need to clone this project first.

```
git clone git@bitbucket.org:valterhenrique/throttling.git
```

Access the project's folder, and choose one of the following methods to run this project.

## Configuration

You can adjust the number of requests that will be made by the application at `Throttling/src/main/resources/application.properties`

    app.NumberOfRequests=10

## How to run

You have 2 options to run this project:

* Maven and Java
* Docker

### Maven and Java

Be sure to have maven >= 3.3.* and java 8 installed in your system.

    mvn spring-boot:run

### Docker

Be sure to have docker installed in your system.

    ./build-dockerfile.sh

## Authors

* **Valter Henrique** - [GitHub](https://github.com/valterhenrique)
