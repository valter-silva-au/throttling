package com.futeam.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Success",
        "Result"
})
public class ConfirmationRequest {

    @JsonProperty("Success")
    private Boolean success;

    @JsonProperty("Result")
    private ConfirmationRequestResult result;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Success")
    public Boolean getSuccess() {
        return success;
    }

    @JsonProperty("Success")
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ConfirmationRequest withSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    @JsonProperty("Result")
    public ConfirmationRequestResult getResult() {
        return result;
    }

    @JsonProperty("Result")
    public void setResult(ConfirmationRequestResult result) {
        this.result = result;
    }

    public ConfirmationRequest withResult(ConfirmationRequestResult result) {
        this.result = result;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ConfirmationRequest withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return "ConfirmationRequest{" +
                "success=" + success +
                ", result=" + result +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}