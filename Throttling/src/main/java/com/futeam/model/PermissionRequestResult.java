package com.futeam.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Availability",
        "TotalRequestNoBefore",
        "UtilizationRate",
        "RequestID"
})
public class PermissionRequestResult {

    @JsonProperty("Availability")
    private Boolean availability;

    @JsonProperty("TotalRequestNoBefore")
    private Integer totalRequestNoBefore;

    @JsonProperty("UtilizationRate")
    private Double utilizationRate;

    @JsonProperty("RequestID")
    private Integer requestID;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Availability")
    public Boolean getAvailability() {
        return availability;
    }

    @JsonProperty("Availability")
    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public PermissionRequestResult withAvailability(Boolean availability) {
        this.availability = availability;
        return this;
    }

    @JsonProperty("TotalRequestNoBefore")
    public Integer getTotalRequestNoBefore() {
        return totalRequestNoBefore;
    }

    @JsonProperty("TotalRequestNoBefore")
    public void setTotalRequestNoBefore(Integer totalRequestNoBefore) {
        this.totalRequestNoBefore = totalRequestNoBefore;
    }

    public PermissionRequestResult withTotalRequestNoBefore(Integer totalRequestNoBefore) {
        this.totalRequestNoBefore = totalRequestNoBefore;
        return this;
    }

    @JsonProperty("UtilizationRate")
    public Double getUtilizationRate() {
        return utilizationRate;
    }

    @JsonProperty("UtilizationRate")
    public void setUtilizationRate(Double utilizationRate) {
        this.utilizationRate = utilizationRate;
    }

    public PermissionRequestResult withUtilizationRate(Double utilizationRate) {
        this.utilizationRate = utilizationRate;
        return this;
    }

    @JsonProperty("RequestID")
    public Integer getRequestID() {
        return requestID;
    }

    @JsonProperty("RequestID")
    public void setRequestID(Integer requestID) {
        this.requestID = requestID;
    }

    public PermissionRequestResult withRequestID(Integer requestID) {
        this.requestID = requestID;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PermissionRequestResult withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return "Result{" +
                "availability=" + availability +
                ", totalRequestNoBefore=" + totalRequestNoBefore +
                ", utilizationRate=" + utilizationRate +
                ", requestID=" + requestID +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}