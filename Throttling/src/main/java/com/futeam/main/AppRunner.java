package com.futeam.main;

import com.futeam.model.ConfirmationRequest;
import com.futeam.model.PermissionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

// Calls that service 3 times to demonstrate the method is executed asynchronously.

@Component
public class AppRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(AppRunner.class);

    @Value("${app.NumberOfRequests}")
    private int numberOfRequests;

    private final ThrottlingService throttlingService;

    public AppRunner(ThrottlingService throttlingService) {
        this.throttlingService = throttlingService;
    }

    @Override
    public void run(String... args) throws Exception {
        // Start the clock
        long start = System.currentTimeMillis();

        LocalDateTime globalTimeStamp = throttlingService.whatTimeIsNow();
        logger.info("Global timestamp for our services: " + globalTimeStamp);

        // TODO (vasi) : use jedis to communicate with Redis
        // Jedis jedis = new Jedis("localhost");
        // jedis.hincrBy("SystemStatistics:atomicRequestID", "statisticValue", 1);
        // jedis.set ..
        // jedis.get ..

        int initialRequestId = throttlingService.retrieveInitialId();
        logger.info("Initial Request ID retrieved:" + initialRequestId);

        logger.info("Number of requests that will be generated: " + numberOfRequests);

        int counter = 0;
        List<CompletableFuture<PermissionRequest>> permissions = new ArrayList<>();
        for (int i = 0; i < numberOfRequests; i++) {
            // Kick of multiple, asynchronous as permission requests
            permissions.add(throttlingService.askPermission(initialRequestId + counter, false));
            counter++;
            // avoid reaching API limit
            Thread.sleep(1000);
        }

        // Wait until they are all done
        allAsList(permissions).join();

        // Print results
        for (CompletableFuture<PermissionRequest> permission: permissions){
            logger.info("--> " + permission.get());
        }

        counter = 0;
        List<CompletableFuture<ConfirmationRequest>> confirmations = new ArrayList<>();
        for (int i = 0; i < numberOfRequests; i++) {
            // Kick of multiple, asynchronous send confirmation requests
            confirmations.add(throttlingService.sendConfirmation(initialRequestId + counter, true));
            counter++;
            Thread.sleep(1000);
        }

        allAsList(confirmations).join();

        for (CompletableFuture<ConfirmationRequest> confirmation: confirmations){
            logger.info("--> " + confirmation.get());
        }

        logger.info("Elapsed time: " + (System.currentTimeMillis() - start));

    }

    private <T> CompletableFuture<List<T>> allAsList(final List<CompletableFuture<T>> futures) {
        return CompletableFuture.allOf(
                futures.toArray(new CompletableFuture[futures.size()])
        ).thenApply(ignored ->
                futures.stream().map(CompletableFuture::join).collect(Collectors.toList())
        );
    }

}
