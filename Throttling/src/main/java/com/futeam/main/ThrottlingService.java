package com.futeam.main;

import com.futeam.model.ConfirmationRequest;
import com.futeam.model.GlobalTimeStamp;
import com.futeam.model.PermissionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;


@Service
public class ThrottlingService {

    private static Logger logger = LoggerFactory.getLogger(ThrottlingService.class);

    @Value("${app.Endpoint}")
    private String endpoint;

    @Value("${app.AuthKey}")
    private String authKey;

    @Value("${app.DestinationEndpoint}")
    private String destinationEndpoint;

    @Value("${timestamp.Endpoint}")
    private String timeStampEndpoint;

    @Value("${timestamp.Parameters}")
    private String timeStampParams;

    private final RestTemplate restTemplate;

    public ThrottlingService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    LocalDateTime whatTimeIsNow(){
        String url = timeStampEndpoint;
        if (!StringUtils.isEmpty(timeStampParams)){
            url += timeStampParams;
        }

        String epoch = restTemplate.getForObject(url, GlobalTimeStamp.class).getTimestamp();

        // convert epoch to LocalDateTime
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(epoch)), ZoneId.systemDefault());
    }

    Integer retrieveInitialId() throws InterruptedException {
        verifyOrGenerateDestinationEndpoint();
        verifyOrGenerateAuthKey();
        logger.info("Retrieving Initial Request ID");
        PermissionRequest request = restTemplate.getForObject(buildURL(-1,false), PermissionRequest.class);
        return request.getResult().getRequestID();
    }

    @Async
    CompletableFuture<PermissionRequest> askPermission(int requestId, boolean forConfirmation) throws InterruptedException {
        verifyOrGenerateDestinationEndpoint();
        verifyOrGenerateAuthKey();
        logger.info("Asking for permission: " +
                "requestId='" + requestId + "', " +
                "forConfimation='" + forConfirmation + "', " +
                "authKey='" + authKey + "', " +
                "destinationEndpoint='" + destinationEndpoint);

        PermissionRequest request = restTemplate.getForObject(buildURL(requestId,forConfirmation), PermissionRequest.class);
        return CompletableFuture.completedFuture(request);
    }

    @Async
    CompletableFuture<ConfirmationRequest> sendConfirmation(int requestId, boolean forConfirmation) throws InterruptedException {
        logger.info("Sending confirmation for: " + requestId);
        ConfirmationRequest request = restTemplate.getForObject(buildURL(requestId, forConfirmation), ConfirmationRequest.class);
        return CompletableFuture.completedFuture(request);
    }

    private String buildURL(int requestId, boolean forConfirmation){
        return String.format(endpoint + "/" +
                "?request_id=%d" +
                "&for_confirmation=%b" +
                "&destination_endpoint=%s" +
                "&auth_key=%s", requestId, forConfirmation, destinationEndpoint, authKey);
    }

    // destination_endpoint: Base64 encoded endpoint. Should contain tfuteam.com domain, rest can be randomly set
    private void verifyOrGenerateDestinationEndpoint() {

        if (StringUtils.isEmpty(destinationEndpoint)){
            String randomString = generateRandomString();
            String randomDestinationEndpoint = String.format("%s.%s", "tfuteam.com", randomString);
            destinationEndpoint = Base64.getEncoder().encodeToString(randomDestinationEndpoint.getBytes());
        }

        if (isBase64(destinationEndpoint)){
            String decoded = decodeBase64(destinationEndpoint.getBytes());
            if (!decoded.contains("tfuteam.com")){
                throw new IllegalStateException("destinationEndpoint does not contain tfuteam.com domain");
            }
        } else {
            throw new IllegalStateException("destionationEndpoint was not encoded to base64 properly");
        }

    }

    // auth_key: Base64 encoded unique auth key.
    // Used for monitoring. Can be randomly set for each request to a API endpoint.
    private void verifyOrGenerateAuthKey(){
        if (StringUtils.isEmpty(authKey)) {
            SecureRandom secureRandom = new SecureRandom();
            // 130 bits from a cryptographically secure random bit generator
            // Encoding them in base-32. 128 bits is considered to be cryptographically strong, but each digit in a
            // base 32 number can encode 5 bits, so 128 is rounded up to the next multiple of 5.
            // This encoding should be compact and efficient, with 5 random bits per character
            String randomAuthKey = new BigInteger(130, secureRandom).toString(32);
            // randomAuthKey needs to be encoded into Base64
            authKey = Base64.getEncoder().encodeToString(randomAuthKey.getBytes());
        }

        if (!isBase64(authKey)){
            throw new IllegalStateException("Invalid base64 content for authKey");
        }
    }

    private boolean isBase64(String encoded) {
        try{
            if (encoded.isEmpty() || encoded.length() % 4 != 0) {
                return false;
            }

            Base64.Decoder decoder = Base64.getDecoder();
            decoder.decode(encoded);
        } catch (StringIndexOutOfBoundsException | IllegalArgumentException e) {
            // invalid base64 received
            return false;
        }
        return true;
    }

    private String decodeBase64(byte[] bytes) {
        return new String(Base64.getDecoder().decode(bytes), StandardCharsets.UTF_8);
    }

    private String generateRandomString(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}
