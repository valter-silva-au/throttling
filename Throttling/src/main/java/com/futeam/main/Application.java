package com.futeam.main;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableAsync
public class Application {

    @Value("${sysflags.MaxParallelQueryNo}")
    private int maxPoolSize;

    @Value("${sysflags.MaxAllowedExecutionTimeMS}")
    private int keepAliveMS;

    @Value("${sysflags.QueueCapacity}")
    private int queueCapacity;

    public static void main(String[] args) {
        // close the application context to shut down the custom ExecutorService
        SpringApplication.run(Application.class, args).close();
    }

    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setAllowCoreThreadTimeOut(true);
        executor.setKeepAliveSeconds(keepAliveMS/1000);

        // short name for ThrottlingService, due to the logging limitation
        executor.setThreadNamePrefix("ThrottlingSrv-");
        executor.initialize();
        return executor;
    }

}
