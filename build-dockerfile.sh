#!/bin/bash

set -x

docker build -f Dockerfiles/Java8 -t futeam/oracle-java:8 .
docker build -f Dockerfiles/Maven3 -t futeam/maven:3.3-jdk-8 .

cd "./Throttling"
docker run -it --rm -v "$PWD":/app -w /app futeam/maven:3.3-jdk-8 mvn spring-boot:run
